import React, { Component } from 'react';

export default class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = { name: '' };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) { this.setState({ name: event.target.value }); }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                    Name:
            <input type="text" value={this.state.name} onChange={this.handleChange} />        </label>
                <input type="submit" value="Submit" /><br />
                {this.state.name}
            </form>
        );
    }
}